<?php

namespace Lexik\Bundle\TopOrFlopBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Categorie
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Entity
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Lexik\Bundle\TopOrFlopBundle\Repository\CategorieRepository")
 * @UniqueEntity(fields="label", message="Cette catégorie existe")
 */
class Categorie
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $label
     *
     * @ORM\Column(name="label", type="string", length=255, unique = true)
     *
     * @Assert\NotBlank()
     */
    private $label;


    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(
     *     targetEntity="Lexik\Bundle\TopOrFlopBundle\Entity\Media",
     *     mappedBy="categorie",
     *     cascade={"persist"}
     * )
     *
     * @Serializer\Exclude()
     */
    private $medias;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->medias = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    function getLabel() {
        return $this->label;
    }

    function setLabel($label) {
        $this->label = $label;
    }




    /**
     * Add medias
     *
     * @param \Lexik\Bundle\TopOrFlopBundle\Entity\Media $medias
     * @return Categorie
     */
    public function addMedia(\Lexik\Bundle\TopOrFlopBundle\Entity\Media $medias)
    {
        $this->medias[] = $medias;
    
        return $this;
    }

    /**
     * Remove medias
     *
     * @param \Lexik\Bundle\TopOrFlopBundle\Entity\Media $medias
     */
    public function removeMedia(\Lexik\Bundle\TopOrFlopBundle\Entity\Media $medias)
    {
        $this->medias->removeElement($medias);
    }

    /**
     * Get medias
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMedias()
    {
        return $this->medias;
    }
}