<?php

namespace Lexik\Bundle\TopOrFlopBundle\Service;

use Doctrine\ORM\EntityManager;

use Symfony\Component\Security\Core\SecurityContext;

use Lexik\Bundle\TopOrFlopBundle\Entity\Categorie;


/**
 * Class CategorieManager
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Service
 */
class CategorieManager {
    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var SecurityContext
     */
    private $context;

    /**
     * Constructor
     *
     * @param EntityManager   $entityManager
     * @param SecurityContext $securityContext
     */
    public function __construct(EntityManager $entityManager, SecurityContext $securityContext)
    {
        $this->em = $entityManager;
        $this->context = $securityContext;
    }
    /**
     * Get a media object from an id, with votes hydrated
     *
     * @param  integer $id
     * @return Media
     */
    public function getCategorie($id)
    {
        $categorieRepository = $this->em->getRepository('Lexik\Bundle\TopOrFlopBundle\Entity\Categorie');
        $categorie = $categorieRepository->find($id);

        return $categorie instanceof Categorie ? $categorie : null;
    }
   
}
