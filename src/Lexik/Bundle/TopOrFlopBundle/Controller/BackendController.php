<?php

namespace Lexik\Bundle\TopOrFlopBundle\Controller;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Form\MediaType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class BackendController
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Controller
 *
 * @Route("/backend")
 */
class BackendController extends Controller
{
    /**
     * @Route("/media/new", name="backend_new")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function newAction(Request $request)
            
    {
        
        $em = $this->getDoctrine()->getManager();
        /* test sur l'existance des categories*/
        $categorie = $em->getRepository('LexikTopOrFlopBundle:Categorie')->findAll();
        if($categorie == Null){
            $this->get('session')->getFlashBag()->add('CategorieVide', 'Aucune Catégorie trouvé! il faut ajouter une');
             /* si aucune categirie redirection vers creation d'un catégorie*/
            return $this->redirect($this->generateUrl('categorie_new'));
        }
        $media = new Media();

        $form = $this->createForm(new MediaType(), $media);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->get('doctrine.orm.entity_manager');
            $em->persist($media);
            $em->flush();
$this->get('session')->getFlashBag()->add('successAjout', 'Votre Media est enregistré');
            return $this->redirect($this->generateUrl('backend_new'));
        }

        return $this->render('LexikTopOrFlopBundle:Backend:new.html.twig', array(
            'form'  => $form->createView(),
        ));
    }
    /**
     * @Route("/", name="media_list")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function indexAction()
    {
       $em = $this->getDoctrine()->getManager();
       $medias = $em->getRepository('LexikTopOrFlopBundle:Media')->findAll();
        return $this->render('LexikTopOrFlopBundle:Backend:index.html.twig', array(
            'medias'  => $medias,
        ));
    }
   /**
     * @Route("/media/{id}/edit", name="media_edit")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function updateAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
       $media = $em->getRepository('LexikTopOrFlopBundle:Media')->find($id);

        if (!$media) {
            throw $this->createNotFoundException('Unable to find Media entity.');
        }
        $editForm = $this->createForm(new MediaType(), $media);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
             $em->merge($media);
            $em->flush();
$this->get('session')->getFlashBag()->add('successModif', 'Votre Media est Modifié');
            return $this->redirect($this->generateUrl('media_list'));
        }
return $this->render('LexikTopOrFlopBundle:Backend:edit.html.twig', array(
            'media'      => $media,
            'edit_form'   => $editForm->createView(),
        ));
        
    }
    /**
     * @Route("/media/{id}/delete", name="media_delete")
     *
     * @param Request $request
     *
     * @return RedirectResponse|Response
     */
    public function deleteAction(Request $request, $id)
    {
      $em = $this->getDoctrine()->getManager();
       $media = $em->getRepository('LexikTopOrFlopBundle:Media')->find($id);

        if (!$media) {
            throw $this->createNotFoundException('Unable to find Media entity.');
        }
        
             $em->remove($media);
            $em->flush();
$this->get('session')->getFlashBag()->add('successdelet', 'Votre Media est supprimé');
            return $this->redirect($this->generateUrl('media_list'));
        
        
    }
}
