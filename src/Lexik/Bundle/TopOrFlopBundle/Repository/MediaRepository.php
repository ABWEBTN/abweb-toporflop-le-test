<?php

namespace Lexik\Bundle\TopOrFlopBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Lexik\Bundle\TopOrFlopBundle\Entity\Media;
use Lexik\Bundle\TopOrFlopBundle\Entity\User;

/**
 * Class MediaRepository
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Repository
 */
class MediaRepository extends EntityRepository
{
    /**
     * Get a random media
     *
     * @return Media
     */
    public function getRandomMedia()
    {
        // note: orderBy('RAND()') not supported in Doctrine2, so 2 queries are required

        // get the media ids
        $ids = $this->createQueryBuilder('m')
                ->select('m.id')
                ->getQuery()
                ->getResult();

        if (!count($ids)) {
            return null;
        }

        // get a random Id from the list
        $index = rand(0, count($ids) - 1);
        $randomId = $ids[$index]['id'];

        // get the media corresponding to this id
        $media = $this->getHydratedMediaById($randomId);

        return $media;
    }

    /**
     * Get a media from an id, with votes hydrated
     *
     * @param integer $id
     *
     * @return Media
     */
    public function getHydratedMediaById($id)
    {
        return $this
            ->createQueryBuilder('m')
            ->select('m, v')
            ->leftJoin('m.votes', 'v')
            ->where('m.id = :randomId')
            ->setParameter('randomId', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * Get 5 highest rated medias
     *
     * Note: The Paginator is used to prevent wrong number of results on query with joins and limit.
     *
     * @return Paginator
     */
    public function getTopMedias()
    {
        $query = $this
            ->createQueryBuilder('m')
            ->select('m, v')
            ->leftJoin('m.votes', 'v')
            ->where('m.average IS NOT NULL')
            ->orderBy('m.average', 'DESC')
            ->setFirstResult(0)
            ->setMaxResults(5)
            ->getQuery();

        return new Paginator($query, true);
    }

    /**
     * Get 5 lowest rated medias
     *
     * Note: The Paginator is used to prevent wrong number of results on query with joins and limit.
     *
     * @return Paginator
     */
    public function getFlopMedias()
    {
        $query = $this
            ->createQueryBuilder('m')
            ->select('m, v')
            ->leftJoin('m.votes', 'v')
            ->where('m.average IS NOT NULL')
            ->orderBy('m.average', 'ASC')
            ->setFirstResult(0)
            ->setMaxResults(5)
            ->getQuery();

        return new Paginator($query, true);
    }

    /**
     * Get a media User hasn't voted for yet
     *
     * @param User $user
     *
     * @return Media|null
     */
    public function getNewMediaForUser(User $user)
    {
        $repoString = 'Lexik\\Bundle\\TopOrFlopBundle\\Entity\\Media';

        $dql = sprintf(
            'SELECT m.id FROM %s m
            WHERE m.id NOT IN (
                SELECT m2.id FROM %s m2
                INNER JOIN m2.votes v2 WITH v2.user = %s
            )',
            $repoString,
            $repoString,
            $user->getId()
        );

        $results = $this
            ->createQueryBuilder('m')
            ->getQuery()
            ->setDQL($dql)
            ->getResult()
        ;

        // return one random media
        if (0 < $count = count($results)) {
            $index = rand(0, $count - 1);
            $id = $results[$index]['id'];

            return $this->getHydratedMediaById($id);
        }

        // or null if none are found
        return null;
    }
}
