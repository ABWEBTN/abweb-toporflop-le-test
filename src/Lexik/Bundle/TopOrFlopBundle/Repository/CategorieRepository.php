<?php

namespace Lexik\Bundle\TopOrFlopBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;

use Lexik\Bundle\TopOrFlopBundle\Entity\Categorie;
use Lexik\Bundle\TopOrFlopBundle\Entity\User;

/**
 * Class CategorieRepository
 *
 * @package Lexik\Bundle\TopOrFlopBundle\Repository
 */
class CategorieRepository extends EntityRepository
{
}
