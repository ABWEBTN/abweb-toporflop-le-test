<?php

namespace Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Lexik\Bundle\TopOrFlopBundle\Entity\Categorie;

/**
 * Class LoadCategorieData
 *
 * @package Lexik\Bundle\TopOrFlopBundle\DataFixtures\ORM
 */
class LoadCategorieData  extends AbstractFixture implements OrderedFixtureInterface{
    
   /**
     * {@inheritdoc}
     */
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i<=5; $i++) {
            $categorie = new Categorie();
            $categorie->setLabel('Catégorie '.$i);
            $manager->persist($categorie);
        }
        $manager->flush();
    }
    /**
     * {@inheritdoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
